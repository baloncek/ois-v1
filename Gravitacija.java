import java.util.*;

public class Gravitacija {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        //System.out.println("OIS je zakon!");
        System.out.println("Vnesite nadmorsko višino");

        
        double C = 6.674 * Math.pow(10, -11);
        double M = 5.972 * Math.pow(10, 24);
        double r = 6.371 * Math.pow(10, 6);
        // nadmorska visina v metrih
        double v = sc.nextDouble();
        // gravitacijski pospesek
        double a = (C * M) / Math.pow((r + v), 2);

        System.out.println("Nadmorska višina je: " + v + " m");
        System.out.println("Pospešek je: " + a);
    }   
}
